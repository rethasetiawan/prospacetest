import { 
  COMPANY_ADDRESS,
  COMPANY_NAME,
  COMPANY_REVENUE,
  COMPANY_CODE,
  COMPANY_PHONE,
  COMPANY_DATA
 } from '../types';

const storeAddress = (address) => ({
  type: COMPANY_ADDRESS,
  payload: address,
});
const storeName = (name) => ({
  type: COMPANY_NAME,
  payload: name,
});
const storeRevenue = (revenue) => ({
  type: COMPANY_REVENUE,
  payload: revenue,
});
const storeCode = (code) => ({
  type: COMPANY_CODE,
  payload: code,
});
const storePhone= (phone) => ({
  type: COMPANY_PHONE,
  payload: phone,
});
const storeData = (data) => ({
  type: COMPANY_DATA,
  payload: data,
});


export { storeAddress,storeName,storeRevenue,storeCode,storePhone,storeData };
