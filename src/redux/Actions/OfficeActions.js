import { 
    OFFICE_NAME,
    OFFICE_LATITUDE,
    OFFICE_LONGTITUDE,
    OFFICE_START,
    OFFICE_DATA,
   } from '../types';
  
  const storeOfficeName = (name) => ({
    type: OFFICE_NAME,
    payload: name,
  });
  const storeOfficeLatitude = (latitude) => ({
    type: OFFICE_LATITUDE,
    payload: latitude,
  });
  const storeOfficeLongtitude = (longtitude) => ({
    type: OFFICE_LONGTITUDE,
    payload: longtitude,
  });
  const storeOfficeStart = (start) => ({
    type: OFFICE_START,
    payload: start,
  });
  const storeOfficeData= (data) => ({
    type: OFFICE_DATA,
    payload: data,
  });

  
  
  export { storeOfficeName,storeOfficeLatitude,storeOfficeLongtitude,storeOfficeStart,storeOfficeData };
  