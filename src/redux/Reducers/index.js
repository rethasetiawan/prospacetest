import { combineReducers } from 'redux';

import CompanyReducers from './CompanyReducers';
import OfficeReducers from './OfficeReducers';

const rootReducer = combineReducers({
  company: CompanyReducers,
  office: OfficeReducers,
});


export default rootReducer;
