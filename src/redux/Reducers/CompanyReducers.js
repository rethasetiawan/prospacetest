import { 
    COMPANY_ADDRESS,
    COMPANY_NAME,
    COMPANY_REVENUE,
    COMPANY_CODE,
    COMPANY_PHONE,
    COMPANY_DATA
   } from '../types';
  
  const INITIAL_STATE = {
    companyName:'',
    companyAddress: '',
    companyRevenue:0,
    companyCode:0,
    companyPhone:0,
    companyData:[],
  };
  
  export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case COMPANY_ADDRESS:
        return {
          ...state,
          companyAddress: action.payload,
        };
    case COMPANY_NAME:
        return {
          ...state,
          companyName: action.payload,
        };
    case COMPANY_REVENUE:
        return {
            ...state,
            companyRevenue: action.payload,
        };
    case COMPANY_CODE:
         return {
              ...state,
              companyCode: action.payload,
        };
    case COMPANY_PHONE:
        return {
             ...state,
             companyPhone: action.payload,
        };   
    case COMPANY_DATA:
        return {
             ...state,
             companyData: action.payload,
        };     
  
      default:
        return state;
    }
  };
  