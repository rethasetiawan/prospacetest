import { 
    OFFICE_DATA,
    OFFICE_NAME,
    OFFICE_LATITUDE,
    OFFICE_LONGTITUDE,
    OFFICE_START,
   } from '../types';
  
  const INITIAL_STATE = {
    officeName:'',
    officeLongtitude: '',
    officeLatitude:0,
    officeStart:'',
    officeData:[],
  };
  
  export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case OFFICE_NAME:
        return {
          ...state,
          officeName: action.payload,
        };
    case OFFICE_LATITUDE:
        return {
          ...state,
          officeLatitude: action.payload,
        };
    case OFFICE_LONGTITUDE:
        return {
            ...state,
            officeLongtitude: action.payload,
        };
    case OFFICE_START:
         return {
              ...state,
              officeStart: action.payload,
        };
    case OFFICE_DATA:
        return {
             ...state,
             officeData: action.payload,
        };   
      default:
        return state;
    }
  };
  