import logo from './logo.svg';
import './App.css';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
// import Routes from './routes/index';
// const Routes = lazy(() => import('../Routes/index'));
import CompanyPage from './containers/CompanyPage';
import OfficePage from './containers/OfficePage';

const App = () => {
  return (
  <Router>
    <Routes>
      <Route path="/" element={ <CompanyPage/>} />
      <Route path="/office" element={ <OfficePage/>} />
    </Routes>
  </Router>
  );
}

export default App;
