/* eslint-disable react/jsx-props-no-spreading */
import React, { lazy, Suspense } from 'react';
import { Routes, Route, useLocation, Redirect } from 'react-router-dom';


import CompanyPage from '../containers/CompanyPage'


const AppRoutes = () => {
  return (
    <div>
        <Routes>
          <Route path={'/'} exact component={CompanyPage} />
        </Routes>
    </div>
  );
};

export default AppRoutes;
