import React, { useState } from 'react';
import { connect } from 'react-redux';
import { storeTextFieldInput } from 'Redux/Actions';
import { colors, generalStyles } from 'Themes';
import './TextField.css';

const TextFieldControlled = (props) => {
  const {
    action,
    marginTop,
    alignBottom,
    title,
    icon,
    asterisk,
    description,
    width,
    disabled,
    placeholder,
    initialValue,
    value,
    belowDescription,
    numberType,
    integerOnly,
    captcha,
    maxLength,
  } = props;

  const [text, setText] = useState(initialValue);

  const handleChange = (event) => {
    const newText = event.target.value;
    if (numberType) {
      if (isFloatNumber(newText) || newText.length === 0) {
        storeTextFieldInput(newText);
        action(newText);
        setText(newText);
      }
    } else if (integerOnly) {
      if (isIntegerOnly(newText) || newText.length === 0) {
        storeTextFieldInput(newText);
        action(newText);
        setText(newText);
      }
    } else if (captcha) {
      if (newText.length <= 6 && (isAlphaNumeric(newText) || newText.length === 0)) {
        storeTextFieldInput(newText);
        action(newText);
        setText(newText);
      }
    } else {
      storeTextFieldInput(newText);
      action(newText);
      setText(newText);
    }
  };

  const isFloatNumber = (str) => {
    const regex = /^\d*\.?\d*$/;
    return regex.test(str);
  };

  const isAlphaNumeric = (str) => {
    const regex = /^[a-z0-9]+$/i;
    return regex.test(str);
  };

  const isIntegerOnly = (str) => {
    const regex = /^\d+$/;
    return regex.test(str);
  };

  return (
    <div
      style={{
        ...generalStyles.regularBlueTextStyle,
        marginTop,
        position: alignBottom ? 'absolute' : 'relative',
        bottom: alignBottom ? 0 : null,
        width: '100%',
      }}
    >
      <div style={styles.titleContainer}>
        <div style={styles.titleStyle}>
          {title}
          <span style={styles.redAsterisk}>{asterisk}</span>
        </div>
        {icon}
      </div>
      <div style={{ ...generalStyles.regularBlackTextStyle }}>{description}</div>

      <div style={{ ...styles.textBox, width }}>
        <input
          disabled={disabled}
          type="text"
          placeholder={placeholder}
          maxLength={maxLength || '150'}
          style={styles.inputStyle}
          onChange={(event) => handleChange(event)}
          value={initialValue ? text : value}
        />
      </div>
      <p style={styles.belowDescription}>{belowDescription}</p>
    </div>
  );
};

const styles = {
  belowDescription: {
    color: '#2d2d2d',
  },
  redAsterisk: {
    color: 'red',
  },
  textBox: {
    height: 'auto',
    borderBottomStyle: 'solid',
    borderWidth: 1,
    borderColor: colors.inputBorderColor,
  },

  titleContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },

  titleStyle: {
    paddingRight: 5,
    lineHeight: 1.88,
  },

  declareContainer: {
    display: 'flex',
    flexDirection: 'row',
    width: 'auto',
    height: 50,
  },

  inputStyle: {
    width: '100%',
    height: 40,
    border: 0,
    outline: 'none',
  },
  placeholderStyle: {
    '::-webkit-input-placeholder': {
      color: 'red',
    },
  },
};

export default connect(null, { storeTextFieldInput })(TextFieldControlled);
