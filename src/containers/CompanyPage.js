import React, { useEffect , useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import closeIcon from '../img/close.jpg';
// import './HomeView.scss'
import { connect } from 'react-redux'
// import {storeAddress} from '../redux/Actions'
import { 
  storeAddress,
  storeName,
  storeRevenue,
  storeCode,
  storePhone,
  storeData 
} from '../redux/Actions/CompanyActions';

const CompanyPage = (props) => {
  let navigate = useNavigate();

  const [data, setData] = useState([]);
  const [ btnCompanyDisable,setBtnCompanyDisable ] = useState(true);
  const [ disableRevenue,setDisableRevenue ] = useState(false);
  const [ disableCode,setDisableCode ] = useState(false);
  const [ disablePhone,setDisablePhone ] = useState(false);

  const addNewCompany = () => {
    const newCompany=  {
      name: props.company.companyName, 
      address: props.company.companyAddress,
      revenue: props.company.companyRevenue,
      code:props.company.companyCode,
      phone:props.company.companyPhone
    };
    setData((data) => [...data, newCompany]);
    props.storeData(data)
 }; 

  useEffect(() => {
  if(
    props.company.companyName &&
    props.company.companyAddress &&
    (props.company.companyRevenue && !disableRevenue) &&
    (props.company.companyCode && !disableCode) &&
    (props.company.companyPhone&& !disablePhone)
    ){
      setBtnCompanyDisable(false)
    } else {
      setBtnCompanyDisable(true)
    }
  });

  const validateNumOnly = (num) => {
    const re = /^[0-9\b]+$/;
    return re.test(num);
  };
  
  const handleChangeName = (event) => {
    props.storeName(event.target.value)
  };
  const handleChangeAddress = (event) => {
    props.storeAddress(event.target.value)
  };
  const handleChangeRevenue = (event) => {
    if ( 0 > event.target.value || !validateNumOnly(event.target.value) ){
      setDisableRevenue(true)
    } else {
      setDisableRevenue(false)
    }
    props.storeRevenue(event.target.value)
  };

  const handleChangeCode= (event) => {
    if ( 0 > event.target.value || event.target.value.length > 3 || !validateNumOnly(event.target.value) ){
      setDisableCode(true)
    }else {
      setDisableCode(false)
    }
    props.storeCode(event.target.value)
  };

  const handleChangePhone= (event) => {
    if ( 0 > event.target.value || !validateNumOnly(event.target.value) ){
      setDisablePhone(true)
    }else {
      setDisablePhone(false)
    }
    props.storePhone(event.target.value)
  };


  const createCompany = () => (
    <div style={{width:'100%'}}>
      <h4>Create Company</h4>
      <div style={styles.bold}>Name:</div>
      <input
          style={styles.textBox}
          type="text"
          placeholder={'name'}
          onChange={(event) => handleChangeName(event)}
       />

      <div style={styles.bold}>Address:</div>
      <input
          style={styles.textBox}
          type="text"
          placeholder={'address'}
          onChange={(event) => handleChangeAddress(event)}
       />
      <div style={styles.bold}>Revenue:</div>
      <input
          style={{...styles.textBox, border: disableRevenue ? '1px solid red' : '1px solid gray'}}
          type="text"
          placeholder={'revenue'}
          onChange={(event) => handleChangeRevenue(event)}
       />
      <div style={styles.bold}>Phone No:</div>
        <div style={{display:'flex',flexDirection:'row'}}>
          <input
              style={{...styles.textBox, border: disableCode ? '1px solid red' : '1px solid gray'}}
              type="text"
              placeholder={'code'}
              onChange={(event) => handleChangeCode(event)}
          />  
          <input
               style={{...styles.textBox, border: disablePhone ? '1px solid red' : '1px solid gray'}}
              type="text"
              placeholder={'number'}
              onChange={(event) => handleChangePhone(event)}
          /> 
        </div>
      <button 
        disabled={btnCompanyDisable}
        style={{...styles.buttonContainer}} 
        onClick={()=>addNewCompany()}>
          Create
      </button>
    </div>
  )
  const deleteCompany = (index)=>{
    data.splice(index,1)
    setData((data) => [...data]);
    props.storeData(data)
  }
  
  const createOffice= () => (
    <div style={{width:'100%'}}>
      <h4>Create Office</h4>
      <div style={styles.bold}>Name:</div>
      <input
          style={styles.textBox}
          type="text"
          placeholder={'name'}
       />
      <div style={styles.bold}>Location:</div>
      <div style={{display:'flex',flexDirection:'row'}}>
        <input
            style={styles.textBox}
            type="text"
            placeholder={'latitude'}
        />
          <input
            style={styles.textBox}
            type="text"
            placeholder={'longtitude'}
        />
       </div>
      <div style={styles.bold}>Office Start Date:</div>
      <input
          style={styles.textBox}
          type="text"
          placeholder={'date'}
       />
      <div style={styles.bold}>Company:</div>
        <input
            style={styles.textBox}
            type="text"
            placeholder={'select company'}
        />  
      <button style={styles.buttonContainer} onClick={()=>{navigate("/office")}}>Create</button>
    </div>
  )

  const companyCard=(name,address,revenue,code,phone,index) =>{
    return(
      <div >
          <div style={{borderBottom:'1px solid black',display:'flex', justifyContent:'space-between'}}>
            <div style={styles.bold}>{name}</div>
            <img style={styles.icon} src={closeIcon} alt="close" onClick={()=>deleteCompany(index)}/>
          </div>
          <div style={styles.bold}>Address:</div>
          {address}
          <div style={styles.bold}>Revenue:</div>
          {revenue}
          <div style={styles.bold}>Phone No:</div>
          ({code}) {phone}
        </div>
    )
  } 

  return (
    <div >
      <div style={{display:'flex', flexDirection:'row'}}>
      {createCompany()}
        <div style={styles.seperatorVertical}/>
      {createOffice()}
    </div>
        <div style={styles.seperatorHorizontal}/>
        
    <h4> Companies</h4>

      <div style={{display:'flex', flexDirection:'row'}}>
      
        {data.map((data, index) => {
          return( 
            <div style={styles.cardContainer} key={index}>
            { companyCard(data.name,data.address,data.revenue,data.code,data.phone,index) }
            </div>
            )}
          )}

      </div>
    
    </div>
  );
};

const styles ={
  buttonContainer:{
      width:'100%',
      height:30,
      marginTop:10,
      border:0,
      borderRadius:5,
      padding:5,
      cursor: 'pointer',
  },
  buttonDisable:{
    border:'1px solid gray',
    background:'transparent'
  },
  textBox:{
    height: 20,
    width: '100%',
    padding:'2px',
    paddingLeft:10,
    borderRadius: 5,
    border: '1px solid gray',
  },
  bold:{
    fontWeight: 'bold',
    marginBottom:10,
    marginTop:5
  },
  seperatorVertical:{
    width:2, 
    height:'auto',
    background: 'gray',
    marginRight: 30,
    marginLeft: 30,
  },
  seperatorHorizontal:{
    width:'auto',
    height:1,
    background: 'gray',
    marginTop: 30,
    marginBottom: 30,
  },
  cardContainer:{
   width :'50%',
   padding:10,
   paddingBottom:20,
   borderRadius: 5,
   border: '1px solid black',
   backgroundColor: 'rgb(247, 247, 247)',
   marginRight: 20,
  },
  icon:{
    width: 20,
    height: 20,
  }
}

const mapStateToProps = state => {
  return {
      company: state.company
  }
}

const mapDispatchToProps = {
  storeAddress,
  storeName,
  storeRevenue,
  storeCode,
  storePhone,
  storeData
};

export default connect(mapStateToProps,mapDispatchToProps)(CompanyPage)
